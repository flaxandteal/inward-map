import Vue from 'vue'
import App from './App.vue'
import { Icon }  from 'leaflet'
import 'leaflet/dist/leaflet.css'
import store from './state/store'

delete Icon.Default.prototype._getIconUrl;

Vue.config.productionTip = false

Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
