import Vue from 'vue'
import Vuex from 'vuex'
import iso3166 from 'iso-3166-1'
import { spatialsankey } from 'd3-spatialsankey'
import * as d3base from 'd3'
const d3 = Object.assign(d3base, { spatialsankey })

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    year: 2018,
    ste: "",
    isSelected: false,
    nodes: {},
    links: [],
    codes: null,
    filter: {},
    articles: {},
    states: {}
  },
  mutations: {
    setFilter (state, filter) {
      state.filter = filter
    },
    setStates (state, states) {
      state.states = states
    },
    setSte (state, ste) {
      state.ste = ste
    },
    setIsSelected (state, isSelected) {
      state.isSelected = isSelected
    },
    setData(state, { nodes, links }) {
      state.nodes = nodes
      state.links = links
    },
    setCodes(state, codes) {
      state.codes = codes
    },
    setArticles(state, articles) {
      state.articles = articles;
    }
  },
  actions: {
    loadArticles: function ({ commit }) {
        return commit('setArticles', []);
    },
    loadCodes: function ({ commit }) {
      return d3.csv("countries-ga-url.csv").then(function(ga_codes) {
        var gaCodeMap = ga_codes.reduce(function (gaCodeMap, code) {
          gaCodeMap[code['ISO-3166-1-alpha2']] = code;
          return gaCodeMap;
        }, {});

        return d3.csv("country-codes.csv").then(function(codes) {
          var codeMap = codes.reduce(function (codeMap, code) {
            var cc = code['ISO3166-1-Alpha-2'];
            codeMap[cc] = code;
            if (cc in gaCodeMap) {
              codeMap[cc].dfaie = gaCodeMap[cc];
            }
            return codeMap;
          }, {});
          commit('setCodes', codeMap);
          return codeMap;
        })
      })
    },
    loadData: function ({ commit, state, dispatch }) {
      return d3.json('nodes.geojson').then(function(nodes) {
        return d3.csv('links.csv').then(function(links) {
          commit('setData', { nodes, links })

          var fillData = function (codes) {
            var matchingLinks = links.filter(
              (link) => link.target === 'GB'
            ).reduce((states, link) => {
              states[link.source] = link.flow;
              return states;
            }, {});

            var states = {};

            states = nodes.features.reduce((states, node) => {
              if (node.id in codes && node.id in matchingLinks) {
                 states[node.id] = {
                   code: codes[node.id],
                   node: node,
                   flow: matchingLinks[node.id]
                 };
              }
              return states;
            }, states);

            commit('setStates', states);

            return states;
          };

          if (state.codes) {
            fillData(state.codes);
          } else {
            return dispatch('loadCodes').then(fillData);
          }

          return { nodes, links }
        })
      })
    }
  },
  getters: {
    statesByFlow: function (state) {
      var filter = state.filter;
      var states = state.states;
      var filteredStates = Object.keys(states).filter(function (ste) {
        if (ste in state.states) {
          for (var key in filter) {
            if (states[ste].code[key] !== filter[key]) {
              return false;
            }
          }
          return true;
        }
      });

      return filteredStates.reduce(function (list, ste) {
        list.push([ste, states[ste].flow]);
        return list;
      }, []).sort(function (a, b) {
        return b[1] - a[1];
      }).map(p => p[0]);
    },
    hasSelectedState: function (state) {
      return state.ste.length && state.isSelected
    },
    steCode: function (state) {
      if (state.ste.length && state.codes) {
        return state.codes[state.ste.toUpperCase()]
      }
      return null
    },
    steItem: function () {
      return ste => {
        if (ste.length) {
          return iso3166.whereAlpha2(ste.toLowerCase())
        }
        return null
      }
    },
    steArticles: function (state) {
      if (state.ste.length && state.ste in state.articles) {
        return state.articles[state.ste]
      }
      return []
    },
    totalPopulation: function (state) {
      if (state.ste && state.states[state.ste]) {
        return state.states[state.ste].flow;
      }
    }
  }
})

export default store
